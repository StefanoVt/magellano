#include "catch.hpp"
#include "MapMinimal.hh"
#include "Utils.hh"
using namespace Minisat;

TEST_CASE("mapping models", "[unit]") {
  SimpleMap m(100);
  MinimalMap mm(100);
  SECTION("initialized instance") {
    REQUIRE(m.getSeed().size() == 0);
    REQUIRE(mm.getSeed().size() == 0);
  }

  SECTION("finding a seed") {
    bool found = m.next();
    REQUIRE(found);
    REQUIRE(m.getSeed().size() > 0);
    found = mm.next();
    REQUIRE(found);
    REQUIRE(mm.getSeed().size() > 0);
    for (size_t a = 0; a < mm.getSeed().size(); a++) {
      REQUIRE(var(mm.getSeed()[a]) == a);
    }
  }

  SECTION("blocking a subspace") {
    vec<Lit> v(100), superset, subset;
    for (size_t i = 0; i < 100; i++) {
      v[i] = (mkLit(i, (i % 3) != 0));
    }
    v.copyTo(subset);
    subset[33] = mkLit(33, true);
    v.copyTo(superset);
    superset[34] = mkLit(34, false);

    SECTION("blocking up") {
      m.blockUp(v);
      REQUIRE(m.hasVisited(v) == true);
      REQUIRE(m.hasVisited(superset) == true);
      REQUIRE(m.hasVisited(subset) == false);
    }

    SECTION("blocking down") {
      m.blockDown(v);
      REQUIRE(m.hasVisited(v) == true);
      REQUIRE(m.hasVisited(subset) == true);
      REQUIRE(m.hasVisited(superset) == false);
    }
  }

  SECTION("exhaust the search space") {}

  SECTION("finding the minimal model available") {
    mm.next();

    REQUIRE(signCount(mm.getSeed()) == 100);

    vec<Lit> allPositive;
    for (int i = 0; i < 100; i++) {
      allPositive.push(mkLit(i, true));
    }
    // printVec(allPositive);
    // printVec(mm.getSeed());
    mm.blockDown(allPositive);
    // eholder();
    mm.next();

    REQUIRE(signCount(mm.getSeed()) == 99);

    for (int i = 0; i < 100; i++) {
      allPositive[i] = mkLit(i, false);
      mm.blockDown(allPositive);
      allPositive[i] = mkLit(i, true);
    }

    mm.next();
    REQUIRE(signCount(mm.getSeed()) == 98);
  }
}
