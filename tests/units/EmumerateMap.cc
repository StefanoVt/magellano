#include "EnumerateMap.hh"
#include "catch.hpp"
#include "Utils.hh"
#include <gflags/gflags.h>
using namespace Minisat;

class EnumerateTest : public EnumerateMap {
public:
  EnumerateTest(int nvars) : EnumerateMap(nvars){};
  virtual bool next() {
    do {
      if (!next_())
        return false;
    } while (hasVisited(lastSeed));
    return true;
  }
};
DEFINE_int32(sizeterm, -1, "term after size");

TEST_CASE("Enumerating map", "[EnumerateMap]") {
  EnumerateMap emap(10);
  REQUIRE(emap.next());
  SECTION("increasing size of sequence") {
    REQUIRE(signCount(emap.getSeed()) == 10);
    REQUIRE(wellFormed(emap.getSeed()));
    for (size_t i = 0; i < 10; i++) {
      emap.next();
      REQUIRE(signCount(emap.getSeed()) == 9);
      REQUIRE(wellFormed(emap.getSeed()));
    }
    emap.next();
    REQUIRE(signCount(emap.getSeed()) == 8);
    REQUIRE(wellFormed(emap.getSeed()));
  }

  SECTION("skipping correctly") {
    EnumerateTest emap2(10);
    REQUIRE(emap2.next());
    int i = 0;
    bool res1, res2;
    do {
      REQUIRE(emap.getSeed().size() == emap2.getSeed().size());
      bool equal = true;
      for (size_t i = 0; i < emap.getSeed().size(); i++) {
        equal = equal && (emap.getSeed()[i] == emap2.getSeed()[i]);
      }
      REQUIRE(equal);
      switch (i++ % 10) {
      case 5:
        emap.blockDown(emap.getSeed());
        emap2.blockDown(emap.getSeed());
        break;
      case 8:
        emap.blockUp(emap.getSeed());
        emap2.blockUp(emap.getSeed());
        break;
      default:
        break;
      }
      res1 = emap.next();
      res2 = emap2.next();
      REQUIRE(res1 == res2);
    } while (res1);
  }

  SECTION("skipping visited parts") {
    vec<Lit> z;
    for (size_t i = 0; i < 10; i++) {
      z.push(mkLit(i, true));
    }
    SECTION("skip everything") {
      // z[0] = mkLit(0);
      placeholder();
      emap.blockUp(z);
      // emap.blockDown(z);
      REQUIRE(!emap.next());
    }
    SECTION("skip second") {
      z[1] = mkLit(1, false);
      emap.blockDown(z);
      REQUIRE(emap.next());
      REQUIRE(!sign(emap.getSeed()[0]));
      REQUIRE(emap.next());
      auto &seed = emap.getSeed();
      REQUIRE(sign(seed[0]));
      REQUIRE(sign(seed[1]));
      REQUIRE(!sign(seed[2]));
    }
  }
}
