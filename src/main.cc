#include <iostream>
#include <memory>
#include <csignal>
#include "MarcoEnum.hh"
#include "minisat/core/Solver.h"
#include "minisat/core/Dimacs.h"
#include <gflags/gflags.h>
using namespace std;
using namespace Minisat;

volatile bool continueLoop = true;

static void printVec(const vec<Lit> &v) {
  cout << "U";
  for (int i = 0; i < v.size(); i++)
    if (!sign(v[i]))
      //  cout << ((i % 2) ? " -" : " ") << (i / 2) + 1;
      cout << " " << i + 1;
  cout << endl;
}

DEFINE_string(solvers, "mini,enum", "available mappers");
DEFINE_string(mapfile, "", "previous map");
DEFINE_uint64(timeout, 0, "timeout");
DEFINE_int32(sizeterm, -1, "term after size");
DEFINE_int32(nvars, -1, "number of vars for MUS");

void sighandler(int _) { continueLoop = false; }

int main(int argc, char *argv[]) {
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  assert(argc > 1);
  // std::cout << argv[1] << std::endl;
  signal(SIGINT, sighandler);
  if (FLAGS_timeout) {
    signal(SIGALRM, sighandler);
    alarm(FLAGS_timeout);
  }
  Minisat::Solver formula;
  parse_DIMACS(gzopen(argv[1], "r"), formula);
  MarcoEnum me(formula, FLAGS_solvers.c_str(), FLAGS_nvars);
  if (!FLAGS_mapfile.empty()) {
    me.preloadMap(gzopen(FLAGS_mapfile.c_str(), "r"));
  }
  while (continueLoop) {
    const EnumOutput &out = me.next();
    if (out.type == EnumOutput::END)
      break;
    printVec(out.vec);
  }
  // cout << "VISITED MAP" << endl;
  // me.saveMap(stdout);
  return 0;
}
