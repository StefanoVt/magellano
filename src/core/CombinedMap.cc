#include "CombinedMap.hh"
#include "MapMinimal.hh"
#include "EnumerateMap.hh"
using namespace Minisat;
using namespace std;
CombinedMap::CombinedMap(int nvars) : wrapper(new EnumerateMap(nvars)) {}

void CombinedMap::blockDown(const Minisat::vec<Minisat::Lit> &c) {
  wrapper->blockDown(c);
}

void CombinedMap::blockUp(const Minisat::vec<Minisat::Lit> &c) {
  wrapper->blockUp(c);
}

bool CombinedMap::hasVisited(const vec<Lit> &m) {
  return wrapper->hasVisited(m);
}

bool CombinedMap::next() {
  static int test = 1000;
  while (test--) {
    return wrapper->next();
  }
  // XXX won't work
  MinimalMap *nm = reinterpret_cast<MinimalMap *>((wrapper.get()));
  unique_ptr<SimpleMap> neww(nm);
  wrapper.swap(neww);
  return wrapper->next();
}

const vec<Lit> &CombinedMap::getSeed() { return wrapper->getSeed(); }
