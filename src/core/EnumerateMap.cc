#include "EnumerateMap.hh"
#include <gflags/gflags.h>

DECLARE_int32(sizeterm);

using namespace Minisat;

static int rightmostTrue(const vec<Lit> &x) {
  for (size_t i = 0; i < x.size(); i++) {
    if (!sign(x[i]))
      return i;
  }
  return x.size();
}

static void increment(vec<Lit> &out, const vec<Lit> &x, int y) {
  x.copyTo(out);
  out.push(mkLit(x.size(), true));
  for (size_t i = y; i < out.size(); i++) {
    out[i] = ~out[i];
    if (!sign(out[i])) { // if now equivalent bit is 1 exit
      break;
    }
  }
}

static vec<Lit> &vecXor(const vec<Lit> &x, const vec<Lit> &y) {
  static vec<Lit> ret;
  int minsize;
  const vec<Lit> *bigger;

  if (x.size() < y.size()) {
    minsize = x.size();
    bigger = &y;
  } else {
    minsize = y.size();
    bigger = &x;
  }

  ret.clear();
  for (size_t i = 0; i < minsize; i++) {
    // 0 == ~l == sign(lLit) and 1 == l == !sign(lLit)
    // 0 0 -> 0 so ~l ~l -> ~l so true true -> true
    // 0 1 -> 1 so ~l  l ->  l so false true -> false
    // 1 0 -> 1 so  l ~l ->  l so true false -> false
    // 1 1 -> 0 so  l  l -> ~l so false false -> true
    ret.push(mkLit(i, sign(x[i]) == sign(y[i])));
  }
  for (size_t i = minsize; i < bigger->size(); i++) {
    ret.push(mkLit(i, sign((*bigger)[i])));
  }

  return ret;
}

static vec<Lit> &vecOr(const vec<Lit> &x, const vec<Lit> &y) {
  static vec<Lit> ret;
  int minsize;
  const vec<Lit> *bigger;

  if (x.size() < y.size()) {
    minsize = x.size();
    bigger = &y;
  } else {
    minsize = y.size();
    bigger = &x;
  }

  ret.clear();
  for (size_t i = 0; i < minsize; i++) {
    // 0 == ~l == sign(lLit) and 1 == l == !sign(lLit)
    // 0 0 -> 0 so ~l ~l -> ~l so true true -> true
    // 0 1 -> 1 so ~l  l ->  l so false true -> false
    // 1 0 -> 1 so  l ~l ->  l so true false -> false
    // 1 1 -> 1 so  l  l -> ~l so false false -> false
    ret.push(mkLit(i, sign(x[i]) && sign(y[i])));
  }
  for (size_t i = minsize; i < bigger->size(); i++) {
    ret.push(mkLit(i, sign((*bigger)[i])));
  }

  return ret;
}

static vec<Lit> &rightShift(const vec<Lit> &x, int y) {
  static vec<Lit> ret;
  ret.clear();
  for (size_t i = 0; i < (x.size() - y); i++) {
    ret.push(mkLit(i, sign(x[i + y])));
  }
  // for (size_t i = x.size() - y; i < x.size(); i++) {
  //  ret.push(mkLit(i, true));
  //}
  return ret;
}

static vec<Lit> &leftShift(const vec<Lit> &x, int y) {
  static vec<Lit> ret;
  ret.clear();
  for (size_t i = 0; i < y; i++) {
    ret.push(mkLit(i, true));
  }
  for (size_t i = 0; i < x.size(); i++) {
    ret.push(mkLit(i + y, sign(x[i])));
  }
  return ret;
}

static bool gosperHack(vec<Lit> &out, const vec<Lit> &in, int &size) {
  // Gosper's hack on vec classes
  // see http://read.seas.harvard.edu/cs207/2012/?p=64
  int y = rightmostTrue(in);
  vec<Lit> c;
  if (y == in.size()) {
    if (&in != &out)
      in.copyTo(out);
    out[0] = mkLit(0, false);
    size++;
    return true;
  }

  increment(c, in, y);
  vec<Lit> &next = vecOr(rightShift((vecXor(in, c)), y + 2), c);
  if (!sign(next.last())) {
    // all subsets of size has been done
    int totsize = in.size();
    size++;
    if (size > in.size())
      return false;
    out.clear();
    for (size_t i = 0; i < size; i++) {
      out.push(mkLit(i, false));
    }
    for (size_t i = size; i < totsize; i++) {
      out.push(mkLit(i, true));
    }
  } else {
    next.pop();
    next.copyTo(out);
  }
  return true;
}
static bool gosperHack(vec<Lit> &out, const vec<Lit> &in) {
  int size = 0;
  int totsize = in.size();
  for (size_t i = 0; i < totsize; i++) {
    if (!sign(in[i]))
      size++;
  }
  return gosperHack(out, in, size);
}

// go to the next value such that seed[pivot] is different
// XXX still buggy
bool EnumerateMap::skip(int pivot) {
  if (pivot == 0) {
    // just do next
    return next_();
  }
  int downCount = 0;
  for (size_t i = 0; i < pivot; i++) {
    if (sign(lastSeed[i])) {
      downCount++;
    }
  }
  if (downCount < pivot) {
    for (size_t i = 0; i < downCount; i++) {
      lastSeed[i] = mkLit(i, true);
    }
    for (size_t i = downCount; i < pivot; i++) {
      lastSeed[i] = mkLit(i, false);
    }
    return next_();
  } else {
    bool ret;
    bool oldsign = sign(lastSeed[pivot]);
    if (!oldsign)
      return next_();
    do {
      ret = next_();
    } while (ret && sign(lastSeed[0]));
    return ret;
  }
}

bool EnumerateMap::next_() {

  if (lastSeed.size() == 0) {
    for (size_t i = 0; i < solver.nVars(); i++) {
      lastSeed.push(mkLit(i, true));
    }
    return true;
  }
  vec<Lit> temp;

  // bool ret = gosperHack(temp, lastSeed);
  // temp.copyTo(lastSeed);
  // return ret;
  return gosperHack(lastSeed, lastSeed, size);
}

bool EnumerateMap::next() {
  if (!next_())
    return false;
  while (hasVisited(lastSeed)) {
    int pivot = -1;
    bool allZeros = true;
    for (size_t i = 0; i < lastSeed.size(); i++) {
      allZeros = allZeros && sign(lastSeed[i]);
      if (solver.conflict.has(~lastSeed[i])) {
        pivot = i;
        if (!allZeros)
          break;
      }
    }
    // assert(pivot >= 0);
    if (pivot > 1) {
      if (!skip(pivot))
        // if (!next_())
        return false;
    } else { //*/
      if (!next_())
        return false;
    }
  }
  if (FLAGS_sizeterm > 0) {
    if (size > FLAGS_sizeterm)
      return false;
  }
  return true;
}
