#include "SimpleMap.hh"
using namespace Minisat;

SimpleMap::SimpleMap(int nvars) : solver() {
  while (nvars--) {
    auto v = solver.newVar();
    solver.freezeVar(v);
  }
}

void SimpleMap::preloadMap(gzFile input) {
  int oldvars = solver.nVars();
  parse_DIMACS(input, solver);
  assert(oldvars == solver.nVars());
}

void SimpleMap::blockUp(const Minisat::vec<Minisat::Lit> &c) {
  vec<Lit> clause;
  for (int i = 0; i < c.size(); i++) {
    if (!sign(c[i])) {
      clause.push(~c[i]);
    }
  }

  solver.addClause(clause);
  solver.eliminate(true);
}

void SimpleMap::blockDown(const Minisat::vec<Minisat::Lit> &c) {
  vec<Lit> clause;
  for (int i = 0; i < c.size(); i++) {
    if (sign(c[i])) {
      clause.push(~c[i]);
    }
  }

  solver.addClause(clause);
  solver.eliminate(true);
}
bool SimpleMap::hasVisited(const Minisat::vec<Minisat::Lit> &model) {
  return !solver.solve(model);
}

const vec<Lit> &SimpleMap::getSeed() { return lastSeed; }

bool SimpleMap::next() {
  bool ret = solver.solve(false);
  if (!ret)
    return false;

  lastSeed.clear();

  for (int i = 0; i < solver.model.size(); i++) {
    lbool value = solver.model[i];
    assert(value != l_Undef);
    lastSeed.push(mkLit(i, value == l_False));
  }

  return true;
}

void SimpleMap::saveMap(FILE *f) {
  vec<Lit> e;
  solver.toDimacs(f, e);
}
