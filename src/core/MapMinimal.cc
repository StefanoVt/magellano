#include "MapMinimal.hh"
using namespace Minisat;

MinimalMap::MinimalMap(int nvars) : SimpleMap(nvars) {
  for (size_t i = 0; i < nvars; i++) {
    solver.setPolarity(i, l_True);
  }
}

bool MinimalMap::next() {
  vec<Lit> negatives, backbone, unexplored;

  bool ret = solver.solve(false);
  if (!ret)
    return false;

  auto &initial = solver.model;
  // XXX better initialization
  for (size_t i = 0; i < initial.size(); i++) {
    if (l_False == (initial[i]))
      negatives.push(mkLit(i, true));
    else
      unexplored.push(mkLit(i, true));
  }
  while (unexplored.size() > 0) {
    Lit &next = unexplored.last();
    // can probably be done with less copying
    vec<Lit> attempt;
    negatives.copyTo(attempt);
    attempt.push(next);
    for (size_t i = 0; i < backbone.size(); i++) {
      attempt.push(backbone[i]);
    }
    auto r = solver.solve(attempt, false);
    if (r) {
      unexplored.clear();
      negatives.clear();
      auto &model = solver.model;
      for (size_t i = 0; i < model.size(); i++) {
        if (l_False == (model[i]))
          negatives.push(mkLit(i, true));
        else {
          bool inBackbone = false;
          for (int j = 0; j < backbone.size(); j++) {
            inBackbone = inBackbone || (var(backbone[j]) == i);
          }
          if (!inBackbone)
            unexplored.push(mkLit(i, true));
        }
      }
    } else {
      backbone.push(~next);
      unexplored.pop();
    }
  }
  lastSeed.clear();
  lastSeed.growTo(solver.nVars());

  for (int i = 0; i < negatives.size(); i++) {
    lastSeed[var(negatives[i])] = negatives[i];
  }
  for (int i = 0; i < backbone.size(); i++) {
    lastSeed[var(backbone[i])] = backbone[i];
  }

  return true;
}
