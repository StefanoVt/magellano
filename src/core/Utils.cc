#include "Utils.hh"
#include <iostream>
using namespace Minisat;
using namespace std;

void placeholder() {}

void printVec(const vec<Lit> &v) {
  cout << "[";
  for (int i = 0; i < v.size(); i++)
    cout << (sign(v[i]) ? "-" : "") << var(v[i]) << ", ";
  cout << "]";
}

int signCount(const vec<Lit> &model) {
  int signcount = 0;
  for (size_t i = 0; i < model.size(); i++) {
    if (sign(model[i]))
      signcount++;
  }
  return signcount;
}

bool wellFormed(const vec<Lit> &model) {
  for (size_t i = 0; i < model.size(); i++) {
    if (var(model[i]) != i)
      return false;
  }
  return true;
}
