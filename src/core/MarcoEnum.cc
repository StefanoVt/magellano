#include "MarcoEnum.hh"
#include "minisat/core/SolverTypes.h"
using namespace std;
using namespace Minisat;

MarcoEnum::MarcoEnum(Solver &f, int nvars)
    : pmap(new EnumerateMap(nvars * 2)), map(*pmap), formula(f) {}

static SimpleMap *switchMap(char const *type, int nvars) {
  auto t = string(type);
  if (t == "enum") {
    return (SimpleMap *)(new EnumerateMap(nvars * 2));
  } else if (t == "mini") {
    return (new MinimalMap(nvars * 2));
  }
  return NULL;
}

MarcoEnum::MarcoEnum(Solver &f, char const *maptype, int nvars)
    : pmap(switchMap(maptype, nvars)), map(*pmap), formula(f) {}

static vec<Lit> &groupsToVars(const vec<Lit> &seed) {
  static vec<Lit> ret;
  ret.clear();
  for (size_t i = 0; i < seed.size(); i++) {
    if (!sign(seed[i])) {
      ret.push(mkLit(i / 2, (i % 2) != 0));
    }
  }
  return ret;
}

const EnumOutput &MarcoEnum::next() {
  do {
    if (!map.next()) {
      out.type = EnumOutput::END;
      break;
    }
  } while (round() != EnumOutput::MUS);
  return out;
}

EnumOutput::OutputType MarcoEnum::round() {
  const vec<Lit> &seed = map.getSeed();
  auto &assumps = groupsToVars(seed);
  bool result = formula.solve(assumps);
  if (result) {
    vec<Lit> mss;
    for (int i = 0; i < (seed.size() / 2); i++) {
      assert(formula.model[i] != l_Undef);
      mss.push(mkLit(i * 2, formula.model[i] == l_False));
      mss.push(mkLit(i * 2 + 1, formula.model[i] == l_True));
    }
    out.type = EnumOutput::MSS;
    map.blockDown(mss);
    // TODO: move in unit test: assert(map.hasVisited(seed));
  } else {
    map.blockUp(seed);
    out.type = EnumOutput::MUS;
    seed.copyTo(out.vec);
  }
  return out.type;
}

void MarcoEnum::preloadMap(gzFile infile) { map.preloadMap(infile); }

void MarcoEnum::saveMap(FILE * outfile) { map.saveMap(outfile); }
