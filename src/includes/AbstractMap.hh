#ifndef ABSTRACT_MAP_HH
#define ABSTRACT_MAP_HH
//#include "minisat/core/Solver.h"
#include "minisat/simp/SimpSolver.h"

class AbstractMap {
public:
  virtual void blockUp(const Minisat::vec<Minisat::Lit> &c) = 0;
  virtual void blockDown(const Minisat::vec<Minisat::Lit> &c) = 0;
  virtual bool hasVisited(const Minisat::vec<Minisat::Lit> &) = 0;
  virtual bool next() = 0;
  virtual const Minisat::vec<Minisat::Lit> &getSeed() = 0;
  virtual bool isMinimal() = 0;
  virtual bool isMaximal() = 0;
};

#endif
