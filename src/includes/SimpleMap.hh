#ifndef SIMPLE_MAP_H
#define SIMPLE_MAP_H
#include "AbstractMap.hh"
#include "minisat/core/Dimacs.h"
#include <iostream>
class SimpleMap : public AbstractMap {
private:
  SimpleMap(const SimpleMap &);

protected:
  Minisat::SimpSolver solver;
  Minisat::vec<Minisat::Lit> lastSeed;

public:
  SimpleMap(int nvars);
  virtual void preloadMap(gzFile input);
  virtual void blockUp(const Minisat::vec<Minisat::Lit> &c);
  virtual void blockDown(const Minisat::vec<Minisat::Lit> &c);
  virtual bool hasVisited(const Minisat::vec<Minisat::Lit> &model);
  virtual bool next();
  virtual const Minisat::vec<Minisat::Lit> &getSeed();
  virtual bool isMinimal() { return false; };
  virtual bool isMaximal() { return false; };
  void saveMap(FILE *);
};

#endif
