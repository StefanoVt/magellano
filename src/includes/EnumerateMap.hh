#ifndef ENUMERATE_MAP_HH
#define ENUMERATE_MAP_HH

#include "SimpleMap.hh"

class EnumerateMap : public SimpleMap {
protected:
  int size = 0;
  bool next_();
  bool skip(int pivot);

public:
  EnumerateMap(int nvars) : SimpleMap(nvars){};
  virtual bool next();
  virtual bool isMinimal() { return true; };
};

#endif
