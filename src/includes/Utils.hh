#ifndef UTILS_HH
#define UTILS_HH
#include "minisat/core/Solver.h"
void placeholder();

void printVec(const Minisat::vec<Minisat::Lit> &v);
int signCount(const Minisat::vec<Minisat::Lit> &model);
bool wellFormed(const Minisat::vec<Minisat::Lit> &model);

#endif
