#ifndef MARCO_ENUM_HH
#define MARCO_ENUM_HH

#include <vector>
#include <memory>
#include "MapMinimal.hh"
#include "EnumerateMap.hh"
#include "minisat/core/Solver.h"

class EnumOutput {
public:
  enum OutputType { END, MUS, MSS } type;
  Minisat::vec<Minisat::Lit> vec;
};

class MarcoEnum {
private:
  std::unique_ptr<SimpleMap> pmap;
  SimpleMap &map;
  Minisat::Solver &formula;
  EnumOutput out;
  void blockDown(const Minisat::vec<Minisat::lbool> &v);
  void blockUp(const Minisat::vec<Minisat::lbool> &v);

public:
  MarcoEnum(Minisat::Solver &f, int nvars);
  MarcoEnum(Minisat::Solver &f, char const *maptype, int nvars);
  const EnumOutput &next();
  EnumOutput::OutputType round();
  void preloadMap(gzFile infile);
  void saveMap(FILE *outfile);
};

#endif
