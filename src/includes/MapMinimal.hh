#ifndef MAP_MINIMAL_HH
#define MAP_MINIMAL_HH
#include "SimpleMap.hh"

class MinimalMap : public SimpleMap
{
public:
  MinimalMap(int nvars);
  virtual bool next();
  virtual bool isMinimal(){return true;};
  virtual bool isMaximal(){return false;};
};


#endif
