#ifndef COMBINED_MAP_HH
#define COMBINED_MAP_HH
#include <memory>
#include "SimpleMap.hh"
//bad idea, do not use
class CombinedMap : public AbstractMap {
protected:
  std::unique_ptr<SimpleMap> wrapper;

public:
  CombinedMap(int nvars);
  virtual void blockUp(const Minisat::vec<Minisat::Lit> &c);
  virtual void blockDown(const Minisat::vec<Minisat::Lit> &c);
  virtual bool hasVisited(const Minisat::vec<Minisat::Lit> &);
  virtual bool next();
  virtual const Minisat::vec<Minisat::Lit> &getSeed();
  virtual bool isMinimal()=0;
  virtual bool isMaximal()=0;
};

#endif
