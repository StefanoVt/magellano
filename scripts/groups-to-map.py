#!/usr/bin/env python2
#transform output of marco script in CNF format, keeping group variables
#used to initialize MARCO map with previously explored groups
import sys

if len(sys.argv) == 1:
    f = sys.stdin
else:
    f = open(sys.argv[1])

outcls = []

for line in f:
    if not line.startswith("U "):
        continue

    line = map(int, line[1:].strip().split())
    vals = [int(v) for v in line]
    outcls.append(vals)

nvars = 182 *2
print 'p cnf', nvars, len(outcls)
for vals in outcls:
    vals = map(str, [-v for v in vals])
    print ' '.join(vals), 0
