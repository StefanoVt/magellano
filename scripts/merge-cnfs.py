#!/usr/bin/env python2
import sys
clauses = set()
maxvar = 0
for fn in sys.argv[1:]:
    with open(fn) as f:
        for line in f:
            if line.startswith('p') or line.startswith('c'):
                continue
            maxvar = max(max(abs(int(x)) for x in line.strip().split()),maxvar)
            clauses.add(line)

#clauses.sort()

print 'p cnf', maxvar, len(clauses)
for clause in clauses:
    print clause.strip()
