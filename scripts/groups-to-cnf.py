#!/usr/bin/env python2
#transform output from MARCO script into a CNF on x
import sys

if len(sys.argv) == 1:
    f = sys.stdin
else:
    f = open(sys.argv[1])

outcls = []

for line in f:
    if not line.startswith("U"):
        continue

    line = map(int, line[1:].strip().split())
    grptox = lambda v: -1*((v %2 )*2-1)*((v+1)//2)
    vals = map(grptox, line)
    outcls.append(vals)

nvars = 182
print 'p cnf', nvars, len(outcls)
for vals in outcls:
    vals = map(str, vals)
    print ' '.join(vals), 0
