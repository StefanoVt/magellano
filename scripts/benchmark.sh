#!/bin/sh

INPUT1=b1input1.tmp
INPUT2=b1input2.tmp
INPUT3=b1input3.tmp
INPUT4=b1input4.tmp
INPUT5=b1input5.tmp
ACCUMX=b1accumx.tmp
ACCUMMAP=b1accummap.tmp
MAGELLANO="../../build/magellano -solvers enum -sizeterm 2 -nvars 182"
MERGE="../../scripts/merge-cnfs.py"
TORESULT="../../scripts/groups-to-cnf.py"
TOMAP="../../scripts/groups-to-map.py"
CHECK="../../scripts/checker.py"

rm -f $INPUT1 $INPUT2 $INPUT3 $INPUT4 $INPUT5 $ACCUMX $ACCUMMAP

$MAGELLANO master-file.cnf > $INPUT1
$TOMAP $INPUT1 > $ACCUMMAP
$TORESULT $INPUT1 > $ACCUMX
rm $INPUT1

for i in $@; do
  $MERGE $i $ACCUMX > $INPUT1
  $MAGELLANO -mapfile $ACCUMMAP $INPUT1 > $INPUT2
  $TORESULT $INPUT2 > $INPUT3
  $CHECK $INPUT1 $INPUT3
  $TOMAP $INPUT2 > $INPUT4
  $MERGE $ACCUMX $INPUT3 > $INPUT5
  mv $INPUT5 $ACCUMX
  $MERGE $ACCUMMAP $INPUT4 > $INPUT5
  mv $INPUT5 $ACCUMMAP
  rm $INPUT1 $INPUT2 $INPUT3 $INPUT4 $INPUT5
done
cat $ACCUMX
