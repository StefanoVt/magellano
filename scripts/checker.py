#!/usr/bin/env python2
import sys
import subprocess
input = sys.argv[1]
outpt = sys.argv[2]

muses = []

with open(outpt) as f:
    for line in f:
        if line.startswith('p cnf'):
            continue
        mus = map(int, line.strip().split())
        assert mus[-1] == 0
        muses.append([-i for i in mus[:-1]])

with open(input) as f:
    head = f.readline().strip().split()
    nvars = (int(head[-2]))
    data = f.read()

for mus in muses:
    minisat = subprocess.Popen("../../build/lib/minisat/minisat",
                stdin=subprocess.PIPE)
    inp = "p cnf {} {}\n".format(nvars, len(data)+len(mus))
    inp += data
    inp += "\n".join("{} 0".format(i) for i in mus)
    minisat.stdin.write(inp)
    minisat.stdin.close()
    minisat.wait()
    print mus, input
    sys.stdout.flush()
