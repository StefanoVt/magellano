cmake_minimum_required(VERSION 3.4)
project(magellano)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")
set(CMAKE_CXX_STANDARD 11)
find_package(Gperftools)
find_package(gflags REQUIRED)
file(GLOB main_SRC
    "src/core/*.cc"
)
file(GLOB test_SRC
    "tests/units/*.cc"
)
include_directories(lib/catch src/includes lib/minisat/)
add_subdirectory(lib/minisat)
enable_testing(true)
add_executable(magellano src/main.cc ${main_SRC})
target_link_libraries(magellano minisat-lib-static gflags ${GPERFTOOLS_LIBRARIES})
add_executable(magellano-test tests/main.cc ${test_SRC} ${main_SRC})
target_link_libraries(magellano-test minisat-lib-static gflags ${GPERFTOOLS_LIBRARIES})
add_test(unit magellano-test)
